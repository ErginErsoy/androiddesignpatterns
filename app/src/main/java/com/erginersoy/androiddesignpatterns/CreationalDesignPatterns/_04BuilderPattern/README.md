**Kurucu Tasarım Kalıbı** olarak ta bilinen Builder Design Pattern, nesneler çok fazla özellik (`attribute`) aldığı zaman, Factory ve Abstract Factory Design pattern'de meydana gelen problemlere çözüm olması için oluşturuldu.

**Factory ve Abstract Factory Design Pattern'de çok fazla attribute olduğu zaman oluşan 3 Önemli Sorun**

* Client tarafından Factory sınıfına çok fazla parametre gönderilmesi hata eğilimlidir. Çünkü çoğu zaman parametrelerin tipi aynıdır ve client tarafında parametrelerin sırasını sürdürebilmek zordur.
* Bazı paremetreler opsiyonel olabilir fakat Factory Design Pattern'de parametreleri gönderilmesi zorunludur ve opsiyonel parametreleri NULL olarak göndeririz.
* Eğer nesne yaratılışı karmaşık ise bu karmaşıklığın tamamı Factory sınıfınında karışıklığına yol açabilir.

Bu durumu zorunlu parametreler için constructor methodu ve opsiyonel parametreler için setter methodları ile çözebiliriz. Fakat bu yaklaşımda oluşturulan nesnenin durumu, opsiyonel paremetrelerin herbirinin setter methodları ile doldurana kadar, tutarsız olacaktır. 

**Builder Pattern** çok fazla parametre ve bu tutarsızlığı; nesneyi adım adım inşa ederek çözer ve inşa edilen nesnenin son halini döndüren bir method sağlar.

Bu Design Pattern genelde Android'de CustomView oluştururken çokca kullanılır.

**JDK'da kullanılan Builder Design Pattern Örnekleri**
* java.lang.StringBuilder#append() (unsynchronized)
* java.lang.StringBuffer#append() (synchronized)
