package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._01Singleton;

/**
 * Created by ErginErsoy on
 * 12.06.2018.
 */
public class _02StaticBlockInitialization {
    private static _02StaticBlockInitialization INSTANCE;

    static {
        try {
            INSTANCE = new _02StaticBlockInitialization();
        } catch (Exception e) {
            throw new RuntimeException("Exception occured in creation singleton instance");
        }
    }

    public static _02StaticBlockInitialization getInstance() {
        return INSTANCE;
    }

    private _02StaticBlockInitialization() {
    }
}
