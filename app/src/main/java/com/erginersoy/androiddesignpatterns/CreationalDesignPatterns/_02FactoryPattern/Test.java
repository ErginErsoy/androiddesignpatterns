package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._02FactoryPattern;

/**
 * Created by ErginErsoy on
 * 12.06.2018.
 */
public class Test {
    public static void main(String[] args) {
        Computer pc = ComputerFactory.getComputer("pc","2 GB","500 GB","2.4 GHz");
        Computer server = ComputerFactory.getComputer("server","16 GB","1 TB","2.9 GHz");
        System.out.println(pc);
        System.out.println(server);
    }
}
