package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._02FactoryPattern;

/**
 * Created by ErginErsoy on
 * 12.06.2018.
 */
public class ComputerFactory {

    public static Computer getComputer(String type, String ram, String hdd, String cpu) {
        if ("PC".equalsIgnoreCase(type)) {
            return new PC(ram, hdd, cpu);
        } else if ("Server".equalsIgnoreCase(type)) {
            return new Server(ram, hdd, cpu);
        }
        return null;
    }
}
