package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._01Singleton;

import java.lang.reflect.Constructor;

/**
 * Created by ErginErsoy on
 * 12.06.2018.
 */
public class _06ReflectionToDestroySingleton {


    //Aşağıdaki kod çalıştırıldığında, Eager... sınıfından oluşan iki örneğin (instance'ın) ve bunların hashcode değerlerinin aynı olmadığını göreceksiniz.
    public static void main(String[] args) {
        _01EagerInitialization instance1 = _01EagerInitialization.getInstance();
        _01EagerInitialization instance2 = null;
        try {
            Constructor[] constructors = _01EagerInitialization.class.getDeclaredConstructors();
            Constructor cons = constructors[0];
            cons.setAccessible(true);
            instance2 = (_01EagerInitialization) cons.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(instance1.hashCode());
        assert instance2 != null;
        System.out.println(instance2.hashCode());
    }
}
