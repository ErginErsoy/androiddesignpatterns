package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._05PrototypePattern;

/**
 * Created by ErginErsoy on
 * 13.06.2018.
 */
public class PrototypePatternTest {
    public static void main(String[] args) throws CloneNotSupportedException {
        Employees employees = new Employees();
        employees.loadData();

        Employees employees1 = (Employees) employees.clone();
        Employees employees2 = (Employees) employees.clone();

        employees1.loadData();
        employees2.getEmployeeList().remove(0);


        System.out.println(employees.getEmployeeList());
        System.out.println(employees1.getEmployeeList());
        System.out.println(employees2.getEmployeeList());
    }
}
