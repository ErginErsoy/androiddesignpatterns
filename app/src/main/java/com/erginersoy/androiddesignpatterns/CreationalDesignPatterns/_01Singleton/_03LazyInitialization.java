package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._01Singleton;

/**
 * Created by ErginErsoy on
 * 12.06.2018.
 */
public class _03LazyInitialization {
    private static _03LazyInitialization INSTANCE;

    public static _03LazyInitialization getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new _03LazyInitialization();
        }
        return INSTANCE;
    }

    private _03LazyInitialization() {
    }
}
