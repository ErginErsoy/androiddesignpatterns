package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._03AbstractFactoryPattern;

/**
 * Created by ErginErsoy on
 * 12.06.2018.
 */
public class ComputerFactory {
    public static Computer getComputer(ComputerAbstractFactory factory) {
        return factory.createComputer();
    }
}
