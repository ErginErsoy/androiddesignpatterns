package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._05PrototypePattern;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ErginErsoy on
 * 13.06.2018.
 */
public class Employees implements Cloneable {

    private List<String> employeeList;

    public Employees() {
        this.employeeList = new ArrayList<>();
    }

    public Employees(List<String> employeeList) {
        this.employeeList = employeeList;
    }

    public List<String> getEmployeeList() {
        return employeeList;
    }
    public void loadData(){
        //Database gibi veri girdilierin olduğu yerden listeyi doldurabilirsiniz.
        employeeList.add("Ergin");
        employeeList.add("Ali");
        employeeList.add("Veli");
        employeeList.add("Mustafa");
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        List<String> tempObj = new ArrayList<>(this.getEmployeeList());
        return new Employees(tempObj);
    }
}
