package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._04BuilderPattern;

/**
 * Created by ErginErsoy on
 * 13.06.2018.
 */
public class BuilderPatternTest {
    public static void main(String[] args) {
        Computer computer = new Computer.ComputerBuilder("16 GB", "500 GB")
                .setBluetoothEnabled(false)
                .setGraphicsCardEnabled(true).build();

        System.out.println(computer);
    }
}
