package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._01Singleton;

/**
 * Created by ErginErsoy on
 * 12.06.2018.
 */
class _04ThreadSafeSingleton_DoubleLocking {
    private static _04ThreadSafeSingleton_DoubleLocking INSTANCE;

    private _04ThreadSafeSingleton_DoubleLocking() {
    }

    /**
     * {@link _04ThreadSafeSingleton} sınıfında olan implementation gayet güzel bir şekilde çalışır.
     * INSTANCE objesini ayrı ayrı oluşturabilecek ilk bir kaç thread için ihtiyaç duymamıza rağmen, komple metodun synchronized olması uygulamanın performansını etkiler.
     * Bu sebeple, bu durumu engellemek için DoubleLocking yaklaşımı kullanılır.
     *
     */
    public static _04ThreadSafeSingleton_DoubleLocking getInstance() {
        if (INSTANCE == null) {
            synchronized (_04ThreadSafeSingleton_DoubleLocking .class){
                if(INSTANCE == null){
                    INSTANCE = new _04ThreadSafeSingleton_DoubleLocking();
                }
            }
        }
        return INSTANCE;

    }
}
