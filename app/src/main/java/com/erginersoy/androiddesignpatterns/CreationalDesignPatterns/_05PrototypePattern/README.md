**Örnek Tasarım Kalıbı** olarak ta bilinen Prototype Design Pattern, nesneyi oluşturmak maliyetli bir işse ve çok fazla süreye ve kaynağa ihtiyaç duyuyorsa ve aynı zamanda benzer bir objeniz var ise kullanılır.

Yani bu tasarım kalıbı orjinal nesnenizi kopyalayarak yeni bir nesne yaratmayı ve bu nesne üzerinde ihtiyaçlara göre değişiklik yapmayı sağlar. Bu tasarım Java dilinin `clone` özelliğini kullanır. 

Bu tasarım kalıbı kopyaladığınız nesneyi kopyalama özelliği olması için zorunlu tutar. Kopyalama işlemi diğer sınıflar içinde yapılmamalıdır. Fakat nesnenin özelliklerinin kopyalaması yüzeysel mi derinlemesine mi olacağı ihtiyaçlara ve tasarım kararına bağlıdır.
  