package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._03AbstractFactoryPattern;

/**
 * Created by ErginErsoy on
 * 12.06.2018.
 */
public class ServerFactory implements ComputerAbstractFactory {
    private String ram;
    private String hdd;
    private String cpu;

    public ServerFactory(String ram, String hdd, String cpu) {
        this.ram = ram;
        this.hdd = hdd;
        this.cpu = cpu;
    }

    @Override
    public Computer createComputer() {
        return new Server(ram, hdd, cpu);
    }
}
