package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._04BuilderPattern;

/**
 * Created by ErginErsoy on
 * 13.06.2018.
 */
public class Computer {

    //Zorunlu parametreler
    private String ram;
    private String hdd;

    //Opsiyonel parametreler
    private boolean isGraphicsCardEnabled;
    private boolean isBluetoothEnabled;

    public String getRam() {
        return ram;
    }

    public String getHdd() {
        return hdd;
    }

    public boolean isGraphicsCardEnabled() {
        return isGraphicsCardEnabled;
    }

    public boolean isBluetoothEnabled() {
        return isBluetoothEnabled;
    }


    private Computer(ComputerBuilder builder) {
        this.hdd = builder.hdd;
        this.ram = builder.ram;
        this.isGraphicsCardEnabled = builder.isGraphicsCardEnabled;
        this.isBluetoothEnabled = builder.isBluetoothEnabled;
    }

    @Override
    public String toString() {
        return "Computer Config : RAM= " + this.getRam() + ", HDD= " + this.getHdd() + ", GPU Enabled= " + this.isGraphicsCardEnabled() + ", Bluetooth= " + this.isBluetoothEnabled();
    }

    public static class ComputerBuilder {
        //Zorunlu parametreler
        private String ram;
        private String hdd;

        //Opsiyonel parametreler
        private boolean isGraphicsCardEnabled;
        private boolean isBluetoothEnabled;

        public ComputerBuilder(String ram, String hdd) {
            this.ram = ram;
            this.hdd = hdd;
        }

        public ComputerBuilder setGraphicsCardEnabled(boolean isGraphicsCardEnabled) {
            this.isGraphicsCardEnabled = isGraphicsCardEnabled;
            return this;
        }

        public ComputerBuilder setBluetoothEnabled(boolean isBluetoothEnabled) {
            this.isBluetoothEnabled = isBluetoothEnabled;
            return this;
        }

        public Computer build() {
            return new Computer(this);
        }
    }


}
