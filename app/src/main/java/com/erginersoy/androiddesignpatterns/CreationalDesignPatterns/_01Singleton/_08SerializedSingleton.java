package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._01Singleton;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by ErginErsoy on
 * 12.06.2018.
 */
class _08SerializedSingleton implements Serializable {
    private static final _08SerializedSingleton INSTANCE = new _08SerializedSingleton();

    static _08SerializedSingleton getInstance() {
        return INSTANCE;
    }

    private _08SerializedSingleton() {
    }


    /*
    *
    * readResolve() Methodunu silerseniz System.out.println() ile çıkan hashCodeların aynı olmadığını göreceksiniz.
    *
    * */

    protected Object readResolve() {
        return INSTANCE;
    }
    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
        _08SerializedSingleton instanceOne = _08SerializedSingleton.getInstance();
        ObjectOutput out = new ObjectOutputStream(new FileOutputStream(
                "filename.txt"));
        out.writeObject(instanceOne);
        out.close();

        //deserailize from file to object
        ObjectInput in = new ObjectInputStream(new FileInputStream(
                "filename.txt"));
        _08SerializedSingleton instanceTwo = (_08SerializedSingleton) in.readObject();
        in.close();

        System.out.println("instanceOne hashCode="+instanceOne.hashCode());
        System.out.println("instanceTwo hashCode="+instanceTwo.hashCode());

    }
}
