package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._01Singleton;

/**
 * Created by ErginErsoy on
 * 12.06.2018.
 */
public class _05BillPughSingleton {

    private _05BillPughSingleton() {
    }

    private static class BillPughSingletonHelper {
        private static final _05BillPughSingleton INSTANCE = new _05BillPughSingleton();
    }


    /**
     * Inner static sınıfı dikkatli bakarsanız {@link _05BillPughSingleton} singleton sınıfının bir örneğini tutar.
     * Singleton sınıfı ilk yüklendiği zaman BillPughSingletonHelper sınıfı hafızaya yüklenmez.
     * Sadece istemci getInstance methodunu çağırdı zaman BillPughSingletonHelper sınıfı yüklenir ve Singleton sınıfının bir örneğini oluşturur.
     * */
    public static _05BillPughSingleton getInstance() {
        return BillPughSingletonHelper.INSTANCE;
    }
}
