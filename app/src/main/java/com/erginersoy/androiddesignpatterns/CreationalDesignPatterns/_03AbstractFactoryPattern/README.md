**Soyut Fabrika Tasarım Kalıbı** olarak ta bilinen Abstract Factory Design Pattern'de Factory Design Pattern'de bulunan if else blokuna göre sub class oluşturmak yerine her subclass için bir factory bulunur.

Abstract Factory sınıfı girilen input factory'e göre sub classları oluşturur. 

**Faydaları**
* Abstract Factory Design Pattern, Interface'ler için implemente etmek yerine kod yazma yaklaşımındadır.
* Abstract Factory Design Pattern, fabrikaların fabrikasıdır. Yeni sınıflar ve yeni ürünler ve yeni fabrikalar kolayca entegre edilebilir. Yapılan örnekte yeni bir alt sınıf olarak Laptop ve fabrika olarak LaptopFactory eklenebilir.
* Abstract Factory Design Pattern, hızlıdır ve şartlı `conditional` mantıklardan kaçınır. (Factory Design Patternde bulunan IF-ELSE gibi)


**JDK'da kullanılan Factory Design Pattern Örnekleri**
* javax.xml.parsers.DocumentBuilderFactory#newInstance()
* javax.xml.transform.TransformerFactory#newInstance()
* javax.xml.xpath.XPathFactory#newInstance()