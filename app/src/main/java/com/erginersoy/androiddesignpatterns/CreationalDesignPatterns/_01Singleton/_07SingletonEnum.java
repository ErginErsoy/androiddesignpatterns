package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._01Singleton;

import java.lang.reflect.Constructor;

/**
 * Created by ErginErsoy on
 * 12.06.2018.
 */
public enum _07SingletonEnum {
    INSTANCE;

    int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public static void main(String[] args) {
        _07SingletonEnum instance1 = _07SingletonEnum.INSTANCE;
        _07SingletonEnum instance2 = null;
        try {
            Constructor[] constructors = _07SingletonEnum.class.getDeclaredConstructors();
            Constructor cons = constructors[0];
            cons.setAccessible(true);
            instance2 = (_07SingletonEnum) cons.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(instance1.hashCode());
        assert instance2 != null;
        System.out.println(instance2.hashCode());
    }
}
