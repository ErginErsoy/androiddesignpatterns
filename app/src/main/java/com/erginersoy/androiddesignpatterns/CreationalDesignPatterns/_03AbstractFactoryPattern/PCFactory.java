package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._03AbstractFactoryPattern;

/**
 * Created by ErginErsoy on
 * 12.06.2018.
 */
public class PCFactory implements ComputerAbstractFactory {
    private String ram;
    private String hdd;
    private String cpu;

    public PCFactory(String ram, String hdd, String cpu) {
        this.ram = ram;
        this.hdd = hdd;
        this.cpu = cpu;
    }

    @Override
    public Computer createComputer() {
        return new PC(ram, hdd, cpu);
    }
}
