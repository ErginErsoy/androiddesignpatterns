package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._03AbstractFactoryPattern;

/**
 * Created by ErginErsoy on
 * 12.06.2018.
 */

/*PC ve Server sınıflarının super class'ı Computer Classıdır.*/
public abstract class Computer {

    public abstract String getRAM();
    public abstract String getHDD();
    public abstract String getCPU();

}
