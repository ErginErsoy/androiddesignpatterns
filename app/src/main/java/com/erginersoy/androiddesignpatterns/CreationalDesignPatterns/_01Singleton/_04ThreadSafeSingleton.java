package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._01Singleton;

/**
 * Created by ErginErsoy on
 * 12.06.2018.
 */
class _04ThreadSafeSingleton {
    private static _04ThreadSafeSingleton INSTANCE;


    //Thread-safe singleton sınıfı oluşturmanın en kolay yolu global erişimi olan methodu (public)
    // synchronized yapmaktır. Bu şekilde method sıralı bir şekilde çalışır.
    public static synchronized _04ThreadSafeSingleton getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new _04ThreadSafeSingleton();
        }
        return INSTANCE;

    }

    private _04ThreadSafeSingleton() {
    }
}
