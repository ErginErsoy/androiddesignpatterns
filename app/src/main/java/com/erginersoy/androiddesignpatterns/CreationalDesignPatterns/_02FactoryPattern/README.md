
**Fabrika Tasarım Kalıbı** olarak ta bilinen Factory Design Pattern, bir sınıfın örneğinin oluşturulmasının sorumluluğunu istemcinden (`client`) factory sınıfına aktarır.
Bu tasarım kalıbı ayrıca **Factory Method Design Pattern** olarak ta bilinir.
Factory Design Pattern'de super class, `interface`, `abstract class` ve ya normal `java class` olabilir.

**Factory Design Pattern Önemli Noktaları**
* Factory sınıfını Singleton olarak tutabiliriz. Ya da altsınıfları (`subclass`) static olarak döndüren methodları tutabiliriz.
* Girilen parametrelere göre, farklı alt sınıfların yaratılır ve döndürülür.

**Avantajları**
* Factory Design Pattern, Interface'ler için implemente etmek yerine kod yazma yaklaşımındadır.
* Factory Design Pattern, istemci tarafında nesne yaratma implementasyonunu kaldırır.
* Factory Design Pattern, kodumuzu daha güçlü, daha az bağımlı ve genişletilmeye,büyümeye daha elverişli yapar. Örneğin; yapılan örnekte `PC` sınıfını değiştirmek çok kolay. Çünkü client program bu durumdan habersizdir.
* İstemci sınıflarında Inheritance sayesinde Implementasyonlar arasında abstraction sağlar.

**JDK'da kullanılan Factory Design Pattern Örnekleri**
* java.utli.Calendar, ResourceBundle and NumberFormat `getInstance()` methodu Factory Design Pattern'i kullanır.
* wrapper sınıflarında (Boolean, Integer etc.)`valueOf()` methodu.     