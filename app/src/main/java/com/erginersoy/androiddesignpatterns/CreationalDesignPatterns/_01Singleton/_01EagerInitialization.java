package com.erginersoy.androiddesignpatterns.CreationalDesignPatterns._01Singleton;

/**
 * Created by ErginErsoy on
 * 12.06.2018.
 */
public class _01EagerInitialization {
    private static final _01EagerInitialization INSTANCE = new _01EagerInitialization();

    static _01EagerInitialization getInstance() {
        return INSTANCE;
    }

    // İstemcinin (Client) constructor methodunu kullanmasını engellemek için private method kullanılır.
    private _01EagerInitialization() {
    }
}
