package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._04FlyweightPattern;

import android.graphics.Canvas;
import android.graphics.Color;

/**
 * Created by ErginErsoy on
 * 18.06.2018.
 */
public interface Shape {
    public void draw(int x, int y, int width, int height, String color);
}
