package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._01AdapterPattern;

/**
 * Created by ErginErsoy on
 * 13.06.2018.
 */
public class Volt {
    private int volts;

    public Volt(int volts) {
        this.volts = volts;
    }

    public int getVolts() {
        return volts;
    }

    public void setVolts(int volts) {
        this.volts = volts;
    }
}

