package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._05FacadePattern;

import java.sql.Connection;

/**
 * Created by ErginErsoy on
 * 18.06.2018.
 */
public class MySqlHelper {

    public static Connection getMysqlDBConnection() {
        return null;
    }

    public void mysqlPdfReport(String tableName, Connection con) {
        //Tablodan verileri alır ve pdf raporu üretir
        System.out.println("Mysql PDF Report");
    }

    public void mysqlHtmlReport(String tableName, Connection con) {
        //Tablodan verileri alır ve Html raporu üretir
        System.out.println("Mysql HTML Report");
    }
}

