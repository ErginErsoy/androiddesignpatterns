package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._07DecoratorPattern;

/**
 * Created by ErginErsoy on
 * 18.06.2018.
 */
public interface Car {
    public void assemble();
}
