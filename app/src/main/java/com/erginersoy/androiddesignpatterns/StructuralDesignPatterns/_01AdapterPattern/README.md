Adaptör Tasarım Kalıbı olarak bilinen "Adapter Design Pattern", birbirinden bağımsız ilişkisiz iki arayüzün (`interface`) birlikte çalışması için kullanılır.
Bu iki ilişkisi arayüzü birleştiren nesneye **Adapter** adı verilir.


***Two Way Adapter Pattern**

Adapter pattern uygulanırken iki yaklaşım vardır. **Class Adapter** and **Object Adapter**.  İkiside aynı sonucu oluşturur.

* **Class Adapter:** Java miras(`Inheritance`) kullanır ve kaynak interface genişletilir. (bkz: Socket.java)
* **Object Adapter:**  Java birleştirme (`Composition`) kullanır ve adapter kaynak nesneyi kullanır.


**JDK'da kullanılan Adapter Pattern Örnekleri**
* java.util.Arrays#asList()
* java.io.InputStreamReader(InputStream) (returns a Reader)
* java.io.OutputStreamWriter(OutputStream) (returns a Writer)
