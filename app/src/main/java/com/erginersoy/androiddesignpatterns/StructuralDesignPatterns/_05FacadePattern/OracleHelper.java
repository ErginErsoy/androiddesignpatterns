package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._05FacadePattern;

import java.sql.Connection;

/**
 * Created by ErginErsoy on
 * 18.06.2018.
 */
public class OracleHelper {
    public static Connection getOracleDBConnection() {
        return null;
    }

    public void oraclePdfReport(String tableName, Connection con) {
        //Tablodan verileri alır ve pdf raporu üretir
        System.out.println("Oracle PDF Report");
    }

    public void oracleHtmlReport(String tableName, Connection con) {
        //Tablodan verileri alır ve Html raporu üretir
        System.out.println("Oracle HTML Report");
    }
}
