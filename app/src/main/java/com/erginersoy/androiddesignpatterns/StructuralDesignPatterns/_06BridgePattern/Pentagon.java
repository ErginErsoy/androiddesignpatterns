package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._06BridgePattern;


/**
 * Created by ErginErsoy on
 * 18.06.2018.
 */
public class Pentagon extends Shape {
    public Pentagon(Color c) {
        super(c);
    }

    @Override
    public void applyColor() {
        System.out.print("Pentagon filled with color ");
        color.applyColor();
    }
}
