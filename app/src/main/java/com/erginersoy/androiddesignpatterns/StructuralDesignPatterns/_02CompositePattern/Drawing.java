package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._02CompositePattern;

/**
 * Created by ErginErsoy on
 * 13.06.2018.
 */

import java.util.ArrayList;
import java.util.List;

/**
 * Bu sınıf Composite (bileşik) nesnesi.
 * Composite nesnesi leaf nesnelerinin grubunu içerir.
 * Bu sınıflar ekleme ve silme işlemleri gibi bazı yardımcı methodlar sağlamalıdır.
 * Ayrıca bu sınıfta removeAll() hepsini sil methoduda sağlayabiliriz.
 *
 * NOT :Composite'de leafler gibi Base Component'i implement eder fakat composite leaflerin bir grubunu içerir.
 */
public class Drawing implements Shape {
    private List<Shape> shapes = new ArrayList<>();

    @Override
    public void draw(String fillColor) {
        for (Shape shape : shapes) {
            shape.draw(fillColor);
        }
    }

    public void add(Shape s) {
        this.shapes.add(s);
    }

    public void remove(Shape s) {
        this.shapes.remove(s);
    }

    public void removeAll() {
        this.shapes.clear();
    }
}
