package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._07DecoratorPattern;

/**
 * Created by ErginErsoy on
 * 18.06.2018.
 */
public class SportsCar extends CarDecorator {

    public SportsCar(Car c) {
        super(c);
    }

    @Override
    public void assemble(){
        super.assemble();
        System.out.print(" Adding features of Sports Car.");
    }
}
