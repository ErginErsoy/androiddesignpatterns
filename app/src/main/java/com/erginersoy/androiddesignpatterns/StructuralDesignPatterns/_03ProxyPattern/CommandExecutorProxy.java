package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._03ProxyPattern;

/**
 * Created by ErginErsoy on
 * 14.06.2018.
 */

/**
 * Şimdi sadece "Admin" olan kullanıcılara {@link CommandExecutorImpl} full erişim vereceğiz.
 * <p>
 * Eğer kullanıcı admin değilse kullanıcı limitli sayıda komutu çalıştırabilecek
 */
public class CommandExecutorProxy implements CommandExecutor {

    private boolean isAdmin;
    private CommandExecutor executor;

    CommandExecutorProxy(String userName, String pwd) {
        if ("Ergin".equals(userName) && "Ersoy123@".equals(pwd)) {
            isAdmin = true;
        }
        executor = new CommandExecutorImpl();
    }

    @Override
    public void runCommand(String cmd) throws Exception {
        if (!isAdmin && cmd.trim().startsWith("rm")) {
            throw new Exception("rm command is not allowed for non-admin users.");
        }
        executor.runCommand(cmd);
    }
}
