package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._02CompositePattern;

/**
 * Created by ErginErsoy on
 * 13.06.2018.
 */
public class CompositePatternTest {
    public static void main(String[] args) {
        Shape tri = new Triangle();
        Shape tri1 = new Triangle();
        Shape cir = new Circle();

        Drawing drawing = new Drawing();
        drawing.add(tri1);
        drawing.add(tri1);
        drawing.add(cir);

        drawing.draw("Red");

        drawing.removeAll();

        drawing.add(tri);
        drawing.add(cir);
        drawing.draw("Green");
    }
}
