package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._01AdapterPattern;

/**
 * Created by ErginErsoy on
 * 13.06.2018.
 */
public class AdapterPatternTest {

    public static void main(String[] args) {
        testClassAdapter();
        testObjectAdapter();
    }

    private static void testClassAdapter() {
        SocketAdapter socketAdapter = new SocketClassAdapterImpl();
        Volt v3 = getVolt(socketAdapter, 3);
        Volt v12 = getVolt(socketAdapter, 12);
        Volt v120 = getVolt(socketAdapter, 120);
        System.out.println("v3 Class :" + v3.getVolts());
        System.out.println("v12 Class :" + v12.getVolts());
        System.out.println("v120 Class :" + v120.getVolts());
    }

    private static void testObjectAdapter() {
        SocketAdapter socketAdapter = new SocketObjectAdapterImpl();
        Volt v3 = getVolt(socketAdapter, 3);
        Volt v12 = getVolt(socketAdapter, 12);
        Volt v120 = getVolt(socketAdapter, 120);
        System.out.println("v3 Object  :" + v3.getVolts());
        System.out.println("v12 Object :" + v12.getVolts());
        System.out.println("v120 Object :" + v120.getVolts());
    }


    private static Volt getVolt(SocketAdapter sockAdapter, int i) {
        switch (i) {
            case 3:
                return sockAdapter.get3Volt();
            case 12:
                return sockAdapter.get12Volt();
            case 120:
                return sockAdapter.get120Volt();
            default:
                return sockAdapter.get120Volt();
        }
    }
}
