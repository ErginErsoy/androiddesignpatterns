package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._04FlyweightPattern;

/**
 * Created by ErginErsoy on
 * 18.06.2018.
 */
public class Oval implements Shape {
    private boolean fill;

    public Oval(boolean f) {
        System.out.println("################################");
        System.out.println("Oval nesneni yaratılıyor");
        this.fill = f;
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void draw(int x, int y, int width, int height, String color) {
        System.out.println("################################");
        System.out.println("Oval nesnesi çiziliyor");
        System.out.println("Oval fill: " + fill);
        System.out.println("color: " + color);
        System.out.println("x : " + x);
        System.out.println("y : " + y);
        System.out.println("width : " + width);
        System.out.println("height: " + height);
    }
}
