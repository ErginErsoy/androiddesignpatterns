package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._03ProxyPattern;

/**
 * Created by ErginErsoy on
 * 14.06.2018.
 */
public class CommandExecutorImpl implements CommandExecutor {
    @Override
    public void runCommand(String cmd) throws Exception {
        Runtime.getRuntime().exec(cmd);
        System.out.println("'" + cmd + "' komutu çalıştırıldı");
    }
}
