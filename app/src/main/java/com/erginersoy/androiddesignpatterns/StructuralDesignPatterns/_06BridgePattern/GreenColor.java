package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._06BridgePattern;

/**
 * Created by ErginErsoy on
 * 18.06.2018.
 */
public class GreenColor implements Color {
    @Override
    public void applyColor() {
        System.out.println("Green");
    }
}
