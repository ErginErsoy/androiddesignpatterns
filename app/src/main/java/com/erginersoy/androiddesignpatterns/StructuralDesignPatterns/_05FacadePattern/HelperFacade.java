package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._05FacadePattern;

import java.sql.Connection;

/**
 * Created by ErginErsoy on
 * 18.06.2018.
 */
public class HelperFacade {
    public static void generateReport(DBType dType, ReportType rType, String tableName) {
        Connection con = null;
        switch (dType) {
            case MYSQL:
                con = MySqlHelper.getMysqlDBConnection();
                MySqlHelper mySqlHelper = new MySqlHelper();
                switch (rType) {
                    case HTML:
                        mySqlHelper.mysqlHtmlReport(tableName, con);
                        break;
                    case PDF:
                        mySqlHelper.mysqlPdfReport(tableName, con);
                        break;
                }
                break;
            case ORACLE:
                con = OracleHelper.getOracleDBConnection();
                OracleHelper oracleHelper = new OracleHelper();
                switch (rType) {
                    case HTML:
                        oracleHelper.oracleHtmlReport(tableName, con);
                        break;
                    case PDF:
                        oracleHelper.oraclePdfReport(tableName, con);
                        break;
                }
                break;
        }
    }

    public static enum DBType {
        MYSQL, ORACLE;
    }

    public static enum ReportType {
        HTML, PDF;
    }
}
