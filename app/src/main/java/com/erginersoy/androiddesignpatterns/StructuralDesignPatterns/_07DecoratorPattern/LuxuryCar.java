package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._07DecoratorPattern;

/**
 * Created by ErginErsoy on
 * 18.06.2018.
 */
public class LuxuryCar extends CarDecorator {
    public LuxuryCar(Car c) {
        super(c);
    }

    @Override
    public void assemble(){
        super.assemble();
        System.out.print(" Adding features of Luxury Car.");
    }
}
