Bu tasarım kalıbı istemci uygulamalarının sistem ile iletişimine yardımcı olur.
 Örnek olarak Html,pdf raporları gibi farklı tiplerde raporları için MySql/Oracle gibi veritabanlarını kullanan birçok arayüzü olan uygulamamız olduğunu varsayalım. 
 Farklı veritabanı tipleri işlemek için farklı arayüzlerimiz olacaktır. 
 İstemci bu arayüzleri kullanarak gerekli veritabanı bağlantısını yaptıktan sonra raporları üretecektir. 
 Fakat karmaşıklık arttıkça ve arayüzlerin davranış isimleri kafa karıştırıcı olduğu zaman
  bu arayüzler istemci uygulaması için yönetmesi zor bir hal alacaktır. 
  
 **Gof(Gang of four) ' a göre ;**
 
 
 Facede design pattern, alt sistemlerdeki bir dizi arayüz için birleşik bir arayüz sağlar. Facade pattern alt sistemleri daha kolay kullanılır yapan daha üst düzey bir arayüz tanımlar.
 
 <h3>Facade Pattern'de Önemli Noktalar</h3>
 * Facade Desing Pattern istemci uygulamaları için bir helper sınıfıdır. Alt sistem arayüzlerini istemciden gizlemez. Facade tasarımını kullanmak tamamen istemci koduna bağlıdır.
 * Bu tasarım geliştirme sürecinde herhangi bir aşamada uygulanabilir. Genellikle arayüz sayısı arttıkça ve sistem kompleks bir hal almaya başlandığında kullanılır. 
 * Alt sistem arayüzleri Facade'den habersizdir ve bu arayüzler herhangi bir Facade referansı barındırmamalıdırlar.
 * Bu tasarım kalıbı, aynı tarz arayüzler çin uygulanmalıdır. Bu tasarım kalıbının amacı benzer işler yapan birden fazla arayüz için tek bir arayüz tanımlamaktır.
 * İstemci için daha bir arayüz sağlamak için **Factory Design Pattern** ile **Facade Pattern** sistem birlikte kullanılabilir.
  