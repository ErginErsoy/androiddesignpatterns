package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._06BridgePattern;

/**
 * Created by ErginErsoy on
 * 18.06.2018.
 */
public abstract class Shape {
    //Composition - implementor
    protected Color color;

    public Shape(Color c){
        this.color=c;
    }

    abstract public void applyColor();
}
