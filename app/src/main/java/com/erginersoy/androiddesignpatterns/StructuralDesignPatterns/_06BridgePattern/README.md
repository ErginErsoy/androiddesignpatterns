Hem arayüzde hem arayüzün implementasyonunda da arayüz hiyerarşisi varsa, **Bridge Pattern - Köprü Tasarımı** arayüzleri, implementasyonlarından ayırmak ve implementasyonun detaylarını istemci programdan gizlemek için kullanılır.

Köprü tasarım kalıbının implementasyonu, Kalıtım (`inheriance`) yerine bileşimi (`Composition`) tercih etme fikrini takip eder.

 **Gof(Gang of four) ' a göre ;**
 
 
 Bridge design pattern, Bir soyutlamayı (`abstraction`) kendi implementasyonundan ayırır, böylece ikisi bağımsız olarak değişebilir
 