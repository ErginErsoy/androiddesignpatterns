package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._03ProxyPattern;

/**
 * Created by ErginErsoy on
 * 14.06.2018.
 */
public class ProxyPatternTest {
    public static void main(String[] args) {
        CommandExecutor executor = new CommandExecutorProxy("AdminDegil", "wrong_pwd");
        try {
            executor.runCommand("ls -ltr");
            executor.runCommand(" rm -rf abc.pdf");
        } catch (Exception e) {
            System.out.println("Exception Message::" + e.getMessage());
        }

    }
}
