package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._03ProxyPattern;

/**
 * Created by ErginErsoy on
 * 14.06.2018.
 */
public interface CommandExecutor {
    public void runCommand(String cmd) throws Exception;
}
