package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._02CompositePattern;

/**
 * Created by ErginErsoy on
 * 13.06.2018.
 */

//Base Component of Composite Pattern
/* Bu sınıf leaf(yapraklar) ve composites(bileşikler) için genel methodlar tanımlaması yapar.*/
public interface Shape {
    public void draw(String fillColor);
}
