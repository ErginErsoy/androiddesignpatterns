package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._06BridgePattern;

/**
 * Created by ErginErsoy on
 * 18.06.2018.
 */
public class Triangle extends Shape {
    public Triangle(Color c) {
        super(c);
    }


    @Override
    public void applyColor() {
        System.out.print("Triangle filled with color ");
        color.applyColor();
    }


}
