package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._02CompositePattern;

/**
 * Created by ErginErsoy on
 * 13.06.2018.
 */

/** Leaf(Yaprak) Nesneleri base component'i implement eder ve bu sınıflar bileşikler için yapı taşlarıdır. Birden Fazla leaf nesnesi oluşturulabilir. Bkz: {@link Circle}*/
public class Triangle implements Shape {
    @Override
    public void draw(String fillColor) {
        System.out.println("Drawing Triangle with color " + fillColor);
    }
}
