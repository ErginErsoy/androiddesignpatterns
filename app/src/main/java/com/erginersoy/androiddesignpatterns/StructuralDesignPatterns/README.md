Türkçe adı ile Yapısal tasarım kalıpları olarak bilinen "Structural Design Patterns",
Bir sınıf yapısını birden fazla yöntem ile oluşturmayı sağlar. Örneğin küçük nesnelerden büyük nesneleri oluşturmak için miras (`inherihance`) ve birleştirme (`comosition`) kullanmak

* Adaptör Kalıbı(Adapter Pattern)
* Bileşik Tasarım Kalıbı (Composite Pattern)
* Vekil Tasarım Kalıbı (Proxy Pattern)
* Sinek Siklet Tasarım Kalıbı (Flyweight Pattern)
* Cephe Tasarım Kalıbı (Facade Pattern)
* Köprü Tasarım Kalıbı (Bridge Pattern)
* Dekoratör Tasarım Kalıbı (Decorator Pattern)

