package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._04FlyweightPattern;

import android.graphics.Color;

/**
 * Created by ErginErsoy on
 * 18.06.2018.
 */
public class FlyweightTest  {

    private final int WIDTH;
    private final int HEIGHT;

    private static final String colors[] = { "Red", "Green", "Yellow","Blue","Black","White"};
    private static final ShapeFactory.ShapeType shapes[] = { ShapeFactory.ShapeType.LINE, ShapeFactory.ShapeType.OVAL_FILL, ShapeFactory.ShapeType.OVAL_NOFILL };

    public FlyweightTest(int width, int height){
        this.WIDTH=width;
        this.HEIGHT=height;
        for (int i = 0; i < 20; ++i) {
            Shape shape = ShapeFactory.getShape(getRandomShape());
            shape.draw(getRandomX(), getRandomY(), getRandomWidth(),
                    getRandomHeight(), getRandomColor());
        }
    }

    private ShapeFactory.ShapeType getRandomShape() {
        return shapes[(int) (Math.random() * shapes.length)];
    }

    private int getRandomX() {
        return (int) (Math.random() * WIDTH);
    }

    private int getRandomY() {
        return (int) (Math.random() * HEIGHT);
    }

    private int getRandomWidth() {
        return (int) (Math.random() * (WIDTH / 10));
    }

    private int getRandomHeight() {
        return (int) (Math.random() * (HEIGHT / 10));
    }

    private String getRandomColor() {
        return colors[(int) (Math.random() * colors.length)];
    }

    public static void main(String[] args) {
        FlyweightTest drawing = new FlyweightTest(500,600);
    }
}