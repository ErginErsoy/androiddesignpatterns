package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._04FlyweightPattern;

/**
 * Created by ErginErsoy on
 * 18.06.2018.
 */

import java.util.HashMap;

/**
 * Factory Sınıfı istemci tarafında nesneleri başlatılması için kullanılacaktır.
 * Bu yüzden nesneleri map içerisinde factory sınıfında tutmalıyız ki istemci tarafından ulaşılamasın.
 */
public class ShapeFactory {
    private static final HashMap<ShapeType, Shape> shapes = new HashMap<>();

    public static Shape getShape(ShapeType shapeType) {
        Shape shapeImp = shapes.get(shapeType);
        if (shapeImp == null) {
            switch (shapeType) {
                case LINE:
                    shapeImp = new Line();
                    break;
                case OVAL_FILL:
                    shapeImp = new Oval(true);
                    break;
                case OVAL_NOFILL:
                    shapeImp = new Oval(false);
                    break;
            }
            shapes.put(shapeType, shapeImp);
        }
        return shapeImp;
    }

    public static enum ShapeType {
        OVAL_FILL, OVAL_NOFILL, LINE;
    }
}
