package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._07DecoratorPattern;

/**
 * Created by ErginErsoy on
 * 18.06.2018.
 */
public class DecoratorPatternTest {

    public static void main(String[] args) {
        Car sportsCar = new SportsCar(new BasicCar());
        sportsCar.assemble();
        System.out.println("\n*****");

        Car sportsLuxuryCar = new SportsCar(new SportsCar(new LuxuryCar(new BasicCar())));
        sportsLuxuryCar.assemble();
    }
}
