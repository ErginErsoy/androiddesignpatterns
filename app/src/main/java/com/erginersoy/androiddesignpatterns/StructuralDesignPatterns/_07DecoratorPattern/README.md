Bu tasarım kalıbı, bir nesnenin işlevselliğini (`functionality`) çalışma zamanında (`runtime`) değiştirmek için kullanılır.
Aynı zamanda bu sınıftan yaratılan diğer nesneleri bu değişikliten etkilenmezler.

Bu tasarım kalıbı implementasyon için soyut sınıfları yada arayüzleri Birleşim (`Composition`) ile kullanır.

Kalıtımı ya da birleşimi bir nesnenin davranışını genişletmek için kullanırız bu derleme zamanında (`compile time`) yapılır. Çalışma zamanında (`runtime`) yeni özellikler ekleyemez, var olan davranışları silemeyiz. İşte tam bu noktada **Decorator  Desing Pattern** devreye giriyor.

