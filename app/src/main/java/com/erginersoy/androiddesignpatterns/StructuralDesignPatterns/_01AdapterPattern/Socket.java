package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._01AdapterPattern;

/**
 * Created by ErginErsoy on
 * 13.06.2018.
 */
public class Socket {
    public Volt getVolt() {
        return new Volt(120);
    }
}
