package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._06BridgePattern;

/**
 * Created by ErginErsoy on
 * 18.06.2018.
 */
public class BridgePatternTest {
    public static void main(String[] args) {
        Shape triangle = new Triangle(new RedColor());
        Shape pentagon = new Pentagon(new GreenColor());
        triangle.applyColor();
        pentagon.applyColor();
    }
}
