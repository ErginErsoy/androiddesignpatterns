package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._04FlyweightPattern;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;

/**
 * Created by ErginErsoy on
 * 18.06.2018.
 */
public class Line implements Shape {
    public Line() {
        System.out.println("################################");
        System.out.println("Line nesneni yaratılıyor");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void draw(int x, int y, int width, int height, String color) {
        System.out.println("################################");
        System.out.println("Line nesnesi çiziliyor");
        System.out.println("x : " +x);
        System.out.println("y : " +y);
        System.out.println("width : " +width);
        System.out.println("height: " +height);
        System.out.println("color: " +color);

    }
}
