package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._01AdapterPattern;

/**
 * Created by ErginErsoy on
 * 13.06.2018.
 */
public interface SocketAdapter {
    public Volt get120Volt();

    public Volt get12Volt();

    public Volt get3Volt();
}
