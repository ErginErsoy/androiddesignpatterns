Anlaması en kolay tasarım kalıplarından bir tanesidir.

**Vekil Tasarım Kalıbı** olarak bilinir. 

Proxy Pattern amacı,  açıklamalara göre, Başka bir nesneye kontrollü erişim vermek için bir vekil veya yer tutucu sağlamaktır.
Yani vekil tasarım kalıbı, bir işlevselliğe (`functionality`) kontrollü erişim vermek istediğimiz zaman kullanırız.

Örnek verecek olursak, Sistemde komut çalıştıran bir sınıfımız olduğunu düşünelim, eğer bu sınıfı kullanıyorsak bir problem teşkil etmez. Fakat eğer biz bu programı istemciye (`client`) vermek istersek, bazı problemler oluşabilir. Çünkü İstemci programı bazı sistem dosyalarını silecek komutlar çalıştırabilir veya istemediğimiz bazı ayarları değiştirebilir.

