**Sinek Siklet Tasarım Kalıbı** olarakta bilinir.

Bu tasarım kalıbını bir sınıftan birçok nesne yaratacağımız zaman kullanırız. Her nesenin tükettiği bellek alanı, Mobil cihazlar ve gömülü sistemlerde ki gibi düşük bellekli cihazlar için  yüksek önem arz eder. Sinek Siklet Tasarım kalıbı paylaşımlı nesnelerle belleğe yüklenilmesinin azaltılması için uygulanır.

Java'da `StringPool` implementasyonu Flyweight pattern için en iyi örneklerden biridir.

**Kullanılmadan Önce Düşünülmesi Gereken Faktörler**
* Uygulama tarafından oluşturulacak nesnelerin sayısı çok fazla olmalıdır.
* Nesne oluşturulması bellekte ağır yük oluşturmalı ve zaman almalı.
* Nesnenin özellikleri iç kaynaklı ve dış kaynaklı özellikler olarak bölünebilmeli. Dış kaynaklı özellikler client program tarafından tanımlanmalı.

Dış kaynaklı özelliğe örnek olarak; Çember nesnesinin genişliği ve rengi verilebilir.

FlyWeight Design Patterni uygulayabilmek çin, paylaşımlı nesneleri döndürebilecek bi **Flyweight factory** oluşturmalıyız.

<h3>Flyweight Pattern'de Önemli Noktalar</h3>
* Örnekte istemci kodu nesneleri yaratmak için flyweight factory kullanmaya zorlanmadı. Fakat istemci kodunun flyweight pattern'i kullandığında emin olmak için, istemciyi flyweight tasarım implementasyonunu kullanmaya zorlayabiliriz. Fakat bu tamamen bazı uygulamar için tasarım kararına bağlığır.
* Sinek ağırlığı modeli karmaşıklığı ortaya çıkarır ve paylaşılan nesnelerin sayısı çok büyükse, bellek ile zaman arasında bir ticaret söz konusudur, bu yüzden gerekliliklerimize dayanarak mantıklı bir şekilde kullanmamız gerekir.
* Flyweight tasarım deseni implementasyonu, nesnenin'in içsel özelliklerinin sayısı çok fazla olduğunda ve Factory sınıfının implementasyonu çok kompleks bir yapıysa yararlı değildir.