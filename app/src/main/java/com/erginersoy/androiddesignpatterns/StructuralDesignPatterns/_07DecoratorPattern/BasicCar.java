package com.erginersoy.androiddesignpatterns.StructuralDesignPatterns._07DecoratorPattern;

/**
 * Created by ErginErsoy on
 * 18.06.2018.
 */
public class BasicCar implements Car {
    @Override
    public void assemble() {
        System.out.println("Basic Car");
    }
}
